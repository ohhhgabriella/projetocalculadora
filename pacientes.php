<?php
include_once('index.php');
require 'config.php';
include_once("menu.php");
?>
<table border ="0" width="100%" class="table espacoTopo table-hover">
    <tr>
        <th scope="col">Nome</th>
        <th scope="col">Idade</th>
        <th scope="col">Sexo</th>
        <th scope="col">Peso</th>
        <th scope="col">Altura</th>
        <th scope="col">Atividade física</th>
        <th scope="col">Nutricionista</th>
    </tr>
    <?php
    $sql = "SELECT * FROM paciente where id_nutri=".$_SESSION['id'];
    $sql = $pdo->query($sql);
    if($sql->rowcount()>0){
        foreach($sql -> fetchAll() as $calculadora){
            $sqlNutri = "SELECT * FROM nutricionista WHERE id = " .$calculadora['id_nutri'];
            $sqlNutri = $pdo->query($sqlNutri);
            $nutri = $sqlNutri -> fetch();
            echo '<tr>';
            echo'<td>'.$calculadora['nome'].'</td>';
            echo'<td>'.$calculadora['idade'].'</td>';
            echo'<td>'.$calculadora['sexo'].'</td>';
            echo'<td>'.$calculadora['peso'].'</td>';
            echo'<td>'.$calculadora['altura'].'</td>';
            echo'<td>'.$calculadora['ati'].'</td>';
            echo'<td>'.$nutri['nome'].'</td>';
            echo '<td><a href="editarPaciente.php?id='.$calculadora['id'].'"><button type="button" class="btn corBotao text-white">Editar</button></a> - <a href="calculoPaciente.php?id='.$calculadora['id'].'"><button type="button" class="btn corBotao text-white">Resultado</button></a></td>';
            echo '</tr>';
        }
    }
    ?>
</table>

<?php
include_once("rodape.php");
?>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<link rel="stylesheet" href="bootstrap/css/novocss.css"/>
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css"/>