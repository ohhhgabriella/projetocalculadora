<?php
require 'index.php';
//inclui a conexão
require 'config.php';
include_once("menu.php");


//essa é a segunda etapa que é atualização dos dados do paciente
//checa se apertou no botão update
if (isset($_POST['update'])) {
    $id = $_POST['id'];

    $nome = $_POST['nome'];
    $idade = $_POST['idade'];
    $sexo = $_POST['sexo'];
    $peso = $_POST['peso'];
    $altura = $_POST['altura'];
    $ati = $_POST['ati'];

    // checking empty fields
//    if(empty($nome) || empty($idade) || empty($sexo) || empty($peso) || empty($altura) || empty($ati)) {
//
//        if(empty($nome)) {
//            echo "<font color='red'>Name field is empty.</font><br/>";
//        }
//
//        if(empty($idade)) {
//            echo "<font color='red'>Age field is empty.</font><br/>";
//        }
//
////        if(empty($sexo)) {
////            echo "<font color='red'>Sex field is empty.</font><br/>";
////        }
//
//        if(empty($peso)) {
//            echo "<font color='red'>Weigth field is empty.</font><br/>";
//        }
//
//        if(empty($altura)) {
//            echo "<font color='red'>Heigth field is empty.</font><br/>";
//        }
//
//        if(empty($ati)) {
//            echo "<font color='red'>Ati field is empty.</font><br/>";
//        }
//    } else {
    //updating the table
    $sql = "UPDATE paciente SET nome=:nome, idade=:idade, sexo=:sexo, peso=:peso, altura=:altura, ati=:ati WHERE id=:id";

    $query = $pdo->prepare($sql);

    $query->bindparam(':id', $id);
    $query->bindparam(':nome', $nome);
    $query->bindparam(':idade', $idade);
    $query->bindparam(':sexo', $sexo);
    $query->bindparam(':peso', $peso);
    $query->bindparam(':altura', $altura);
    $query->bindparam(':ati', $ati);

    $query->execute();

    header("Location: pacientes.php");
}
?>
<?php
//pega o id da url
$id = $_GET['id'];

//seleciona os dados desse id particularmente
$sql = "SELECT * FROM paciente WHERE id=:id";
$query = $pdo->prepare($sql);
$query->execute(array(':id' => $id));

while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
    $nome = $row['nome'];
    $idade = $row['idade'];
    $sexo = $row['sexo'];
    $peso = $row['peso'];
    $altura = $row['altura'];
    $ati = $row['ati'];

    $f = isset($sexo) == 1 ? 'checked' : '';
    $m = isset($sexo) == 0 ? 'checked' : '';
}
?>
<div class="container col-md-8 espacoTopo">
    <form method="POST">
        <h3 class="text-center">EDITAR INFORMAÇÕES DO PACIENTE</h3>
        <div class="form-group">
            <label for="name">Nome do paciente</label>
            <input type="text" class="form-control" id="exampleInputName" aria-describedby="nameHelp" name="nome" value="<?php echo $nome; ?>">
        </div>
        <div class="form-group">
            <label for="idade">Idade do paciente</label>
            <input type="text" class="form-control" id="exampleInputAge" aria-describedby="numberHelp" name="idade" value="<?php echo $idade; ?>">
        </div>
        <div>
            <label for="name">Sexo</label>
            <div class="form-group">
                <input type="text" class="form-control" id="exampleInputSexo" aria-describedby="numberHelp" name="sexo" value="<?php echo $sexo; ?>">
                <small>Para sexo masculino 0, para sexo feminino 1.</small>
            </div>
        </div>
        <div class="form-group">
            <label for="peso">Peso</label>
            <input type="text" class="form-control" id="peso" aria-describedby="numberHelp" name="peso" value="<?php echo $peso; ?>">
        </div>
        <div class="form-group">
            <label for="altura">Altura</label>
            <input type="text" class="form-control" id="altura" aria-describedby="numberHelp" name="altura" value="<?php echo $altura; ?>">
        </div>
        <div class="form-group">
            <label for="exampleSelect1">Atividades por semana</label>
            <input class="form-control" id="exampleSelect1" name="ati" value="<?php echo $ati; ?>">        
        </div>
        <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">
        <input type="submit" class="btn text-white" style="background-color: black" name="update" value="Update" onclick="editPaciente()">

    </form>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="bootstrap/js/meujs.js"></script>
<link rel="stylesheet" href="bootstrap/css/novocss.css"/>
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css"/>

