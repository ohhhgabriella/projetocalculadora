<?php
include_once('index.php');
require 'config.php';
include_once("menu.php");

if(isset($_POST['nome']) && empty($_POST['nome']==false)){
  $nome = addslashes($_POST['nome']);
  $idade = addslashes($_POST['idade']);
  $sexo = addslashes($_POST['sexo']);
  $peso = addslashes($_POST['peso']);
  $altura = addslashes($_POST['altura']);
  $ati = addslashes($_POST['ati']);

  $sql = "INSERT INTO paciente SET nome='$nome', idade = '$idade', sexo= '$sexo', peso= '$peso', altura='$altura', ati='$ati', id_nutri=" .$_SESSION['id'];

  $pdo -> query($sql);
}
?>

<div class="container col-md-8 espacoTopo">
  <form action="" method="post">
  <h3 class="text-center">INFORMAÇÕES DO PACIENTE</h3>
    <fieldset>
      <div class="form-group">
        <label for="name">Nome do paciente</label>
        <input type="text" class="form-control" id="exampleInputName" aria-describedby="nameHelp" placeholder="João da Silva" name="nome">
      </div>
      <div class="form-group">
        <label for="idade">Idade do paciente</label>
        <input type="text" class="form-control" id="exampleInputAge" aria-describedby="numberHelp" placeholder="20" name="idade">
      </div>
      <div>
          <label for="name">Sexo</label>
          <div class="form-check">
          <input type="radio" id="customRadio1" name="sexo" class="form-check-input" value="0">
          <label class="form-check-label" for="customRadio1">Masculino</label>
          </div>
          <div class="form-check">
            <input type="radio" id="customRadio2" name="sexo" class="form-check-input" value="1">
            <label class="form-check-label" for="customRadio2">Feminino</label>
          </div>
      </div>
      <div class="form-group">
        <label for="peso">Peso</label>
        <input type="text" class="form-control" id="exampleInputAge" aria-describedby="numberHelp" placeholder="00,00" name="peso">
      </div>
      <div class="form-group">
        <label for="altura">Altura</label>
        <input type="text" class="form-control" id="exampleInputAge" aria-describedby="numberHelp" placeholder="0,00" name="altura">
      </div>
      <div class="form-group">
        <label for="exampleSelect1">Atividades por semana</label>
        <select class="form-control" id="exampleSelect1" name="ati">
          <option value="0">0 dia por semana</option>
          <option value="1">1 dia por semana</option>
          <option value="2">2 dias por semana</option>
          <option value="3">3 dias por semana</option>
          <option value="4">4 dias por semana</option>
          <option value="5">5 dias por semana</option>
          <option value="6">6 dias por semana</option>
          <option value="7">7 dias por semana</option>
        </select>
      </div>
      <button type="submit" class="btn text-white" style="background-color: black" onclick="addPaciente()">Enviar</button>
    </fieldset>
  </form>
</div>

<?php
include_once("rodape.php");
?>


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script type="text/javascript" src="bootstrap/js/meujs.js"></script>
<link rel="stylesheet" href="bootstrap/css/novocss.css"/>
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css"/>
