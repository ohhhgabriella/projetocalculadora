<?php
include_once("index.php");
include_once("menu.php");
require 'config.php';
?>
<div class='container espacoTopo'>
    <h3>Sobre a equipe</h3>
    <p>Somos um grupo de estudantes de Sistemas para Internet do Unipê do terceiro período, nossa equipe é formado
        por Ariel Soares - DBA e Analista, Gabriella Barreto-Desenvolvedora Back-End e Front-End, Phillippe Rocha 
        - Desenvolvedor Back-End e Front-End, Carlos Junior - Gerente de Projetos.</p>
    <p>Estamos desenvolvendo esse projeto para a disciplina de Programaçao Dinâmica para Web, como trabalho de 
        conclusão do terceiro estágio da disciplina. Esse projeto consiste em possibilitar para uma Nutricionista
        uma forma de agilizar o seu trabalho e calcular as informações dos seus pacientes de maneira precisa, além 
        de guardar os dados dos seus pacientes para consulta posterior.
    </p>
</div>
<div class='container col-md-6 espacoTopo'>
<div class='form-group'>
    <form action='#' method='post'>
      <fieldset>
        <h4 class='text-center'>Entre em contato conosco!</h4>
        <div class='form-group'>
          <label for='exampleInputEmail1'>Nome</label>
          <input type='text' class='form-control' id='exampleInputEmail1' aria-describedby='emailHelp' placeholder='Seu nome aqui' name='nomeContato'>
        </div>
        <div class='form-group'>
          <label for='exampleInputEmail1'>Email address</label>
          <input type='email' class='form-control' id='exampleInputEmail1' aria-describedby='emailHelp' placeholder='Enter email' name='email'>
          <small id='emailHelp' class='form-text text-muted'>Seu e-mail nunca será compartilhado.</small>
        </div>
        <div class='form-group'>
          <label for='exampleInputEmail1'>Assunto</label>
          <input type='text' class='form-control' id='exampleInputEmail1' aria-describedby='emailHelp' placeholder='Assunto do contato' name='assunto'>
        </div>
        <div class='form-group'>
          <label for='exampleTextarea'>Mensagem</label>
          <textarea class='form-control' id='mensagem' rows='3' name='mensagem'></textarea>
        </div>
        <button type='submit' class='btn text-white' style='background-color: black'>Enviar!</button>
    </form>
</div>
</div>
</div>

<?php
include_once("rodape.php");
?>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<link rel="stylesheet" href="bootstrap/css/novocss.css"/>
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css"/>
