<?php
include_once("index.php");
require 'config.php';
include_once("menu.php");
?>
<div>
<h3 class="container text-center">RESULTADO DO PACIENTE</h3>
</div>
<?php
//pega o id da url
$id = $_GET['id'];
//seleciona os dados desse id particularmente
$sql = "SELECT * FROM paciente WHERE id=:id";
$query = $pdo->prepare($sql);
$query->execute(array(':id' => $id));
while($row = $query->fetch(PDO::FETCH_ASSOC))
{
    $nome = $row['nome'];
    $idade = $row['idade'];
    $sexo = $row['sexo'];
    $peso = $row['peso'];
    $altura = $row['altura'];
    $ati = $row['ati'];
}
/****** Calculos *********/
/****** Calculo do imc *******/
$imc = $peso/($altura*$altura); 
//para adultos 
    if($imc<18.7 && $idade<=65){
        echo "<div class='text-center'>Magro. O IMC é= $imc</div>";
    }elseif(18.7<=$imc &&  $imc<=23.9 && $idade<=65){
        echo "<div class='text-center'>Normal. O IMC é= $imc</div>";
    }elseif(23.9<$imc && $imc<28.6 && $idade<=65){
        echo "<div class='text-center'>Sobrepeso. O IMC é= $imc</div>";
    }elseif ($imc >= 28.6 && $idade<=65){
        echo "<div class='text-center'>Obesidade. O IMC é= $imc</div>";
    }
//para idosos
    if ($imc<21.9 && $idade>65){
        echo "<div class='text-center'>Baixo peso. O IMC é= $imc</div>";
    }else if($imc>=21.9 && $imc<=27 && $idade>65){
        echo "<div class='text-center'>Normal. O IMC é= $imc</div>";
    }else if($imc>27 && $imc<=32 && $idade>65){
        echo "<div class='text-center'>Sobrepeso. O IMC é= $imc</div>";
    }else if($imc>32 && $idade>65){
        echo "<div class='text-center'>Obesidade. O IMC é= $imc</div>";
    }
/******  Taxa Metabólica Basal ******/
if ($sexo == 0){
    $tmbhomem=66.5+(13.75*$peso)+(5.003*$altura)-(6.775*$idade);
    echo "<div class='text-center'>Taxa Metabólica Basal do Homem =$tmbhomem</div>";
}elseif ($sexo == 1){
    $tmbmulher=655.1+(9.563*$peso)+(1.850*$altura)-(4.676*$idade);
    echo "<div class='text-center'>Taxa Metabólica Basal da Mulher = $tmbmulher</div>";
}else{
    echo "<div class='text-center'>Entrada inválida</div>";
}
/****** Quantidade de calorias necessárias com base na frequência de atividades físicas
 *  (TMB com base na intensidade de atividades físicas) ******/
 //para HOMENS
 if ($sexo== 0 && $ati<1){
    $kcd=$tmbhomem*1.2;
    echo "<div class='text-center'>Calorias Diárias Necessárias para Sendetário=  $kcd</div>";
}elseif ($sexo== 0 && $ati<3 && $ati>=1){
    $kcd=$tmbhomem*1.375;        
    echo "<div class='text-center'>Calorias Diárias Necessárias Leve= $kcd</div>";
}else if ($sexo== 0 && $ati>=3 && $ati<=5){
    $kcd=$tmbhomem*1.55;
    echo "<div class='text-center'>Calorias Diárias Necessárias Moderado= $kcd</div>";
}else if ($sexo== 0 && $ati>5 && $ati<=7){
    $kcd=$tmbhomem*1.725;
    echo "<div class='text-center'>Calorias Diárias Necessárias Pesado= $kcd</div>";
}else if ($sexo== 0){
    $kcd=$tmbhomem*1.9;
    echo "Calorias Diárias Necessárias Pesado= $kcd</div>";
}
//para MULHERES
if ($sexo== 1 && $ati<1){
    $kcdm=$tmbmulher*1.2;
    echo "<div class='text-center'>Calorias Diárias Necessárias para Sendetário $kcdm</div>";
}else if ($sexo== 1 && $ati<3 && $ati>=1){
    $kcdm=$tmbmulher*1.375;
    echo "<div class='text-center'>Calorias Diárias Necessárias Leve $kcdm</div>";
}else if ($sexo== 1 && $ati>=3 && $ati<=5){
    $kcdm=$tmbmulher*1.55;
    echo "<div class='text-center'>Calorias Diárias Necessárias Moderado $kcdm</div>";
}else if ($sexo== 1 && 5<$ati && $ati<=7){
    $kcdm=$tmbmulher*1.725;
    echo "<div class='text-center'>Calorias Diárias Necessárias Pesado $kcdm</div>";
}else if ($sexo== 1){
    $kcdm=$tmbmulher*1.9;
    echo "<div class='text-center'>Calorias Diárias Necessárias Pesado $kcdm</div>";
}
?>
<div class="container">
    <a href="pacientes.php"><button type="button" class="centro btn corBotao text-white">Voltar</button></a>
</div>
<link rel="stylesheet" href="bootstrap/css/novocss.css"/>
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css"/>